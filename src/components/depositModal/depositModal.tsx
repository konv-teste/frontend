import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
} from "@chakra-ui/react";
import { Field, Form, Formik, FormikHelpers } from "formik";
import { api } from "../../services/api";
import { isValidCpf } from "../../utils/validateCPF";

export function DepositModal() {
  Number.isInteger =
    Number.isInteger ||
    function (value) {
      return (
        typeof value === "number" &&
        isFinite(value) &&
        Math.floor(value) === value
      );
    };

  function validateCPF(value: string) {
    let error;
    if (!value) {
      error = "Necessário preencher os campos.";
    } else if (!isValidCpf(value)) {
      error = "CPF inválido.";
    }
    return error;
  }

  function validateAmount(value: string) {
    let error;
    if (!value) {
      error = "Necessário preencher os campos.";
    } else if (!Number.isInteger(Number(value))) {
      error = "Valor inválido. Apenas valores inteiros.";
    }
    return error;
  }

  function handleSubmit(
    actions: FormikHelpers<{ cpf: string; amount: string }>,
    data: { cpf: string; value: number }
  ) {
    api
      .post("/auth/transactions/credit", data)
      .then((res) => {
        actions.setSubmitting(false);
        actions.resetForm();
        actions.setStatus({ success: "Deposito realizado com sucesso!" });
      })
      .catch((err) => {
        actions.setSubmitting(false);
        if (err.response.data.message === "User not found.") {
          return actions.setStatus({ success: "Usuário não encontrado." });
        }
        actions.setStatus({ success: "Erro ao realizar o deposito." });
      });
  }

  return (
    <>
      <Formik
        initialValues={{ cpf: "", amount: "" }}
        onSubmit={(values, actions) => {
          const data = {
            cpf: values.cpf,
            value: Number(values.amount),
          };
          handleSubmit(actions, data);
        }}
      >
        {({ isSubmitting, status }) => (
          <Form>
            <Field name="cpf" validate={validateCPF}>
              {({ field, form }: any) => (
                <FormControl isInvalid={form.errors.cpf && form.touched.cpf}>
                  <FormLabel htmlFor="cpf">CPF para depósito:</FormLabel>
                  <Input {...field} id="cpf" placeholder="Digite aqui..." />
                  <FormErrorMessage>{form.errors.cpf}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
            <Field name="amount" validate={validateAmount}>
              {({ field, form }: any) => (
                <FormControl
                  isInvalid={form.errors.amount && form.touched.amount}
                >
                  <FormLabel htmlFor="amount">
                    Insira a quantia a ser depositada:
                  </FormLabel>
                  <Input {...field} id="amount" placeholder="Digite aqui..." />
                  <FormErrorMessage>{form.errors.amount}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
            <div>{status ? status.success : ""}</div>
            <Button
              mt={4}
              colorScheme="teal"
              isLoading={isSubmitting}
              type="submit"
            >
              Depositar
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
}
