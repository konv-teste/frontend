import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Table,
  TableCaption,
  TableContainer,
  Tbody,
  Td,
  Text,
  Tfoot,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { Field, Form, Formik, FormikHelpers } from "formik";
import { useState } from "react";
import { api } from "../../services/api";
import { isValidCpf } from "../../utils/validateCPF";

interface ITableData {
  balance: number;
  transactions: ITransactions[];
}

interface ITransactions {
  createdAt: string;
  id: string;
  type: string;
  value: number;
  _id: string;
}

export function BalanceModal() {
  const [dataAccount, setDataAccount] = useState<ITableData>({
    balance: 0,
    transactions: [],
  });
  const [isViewTable, setIsViewTable] = useState(false);

  Number.isInteger =
    Number.isInteger ||
    function (value) {
      return (
        typeof value === "number" &&
        isFinite(value) &&
        Math.floor(value) === value
      );
    };

  function validateCPF(value: string) {
    let error;
    if (!value) {
      error = "Necessário preencher os campos.";
    } else if (!isValidCpf(value)) {
      error = "CPF inválido.";
    }
    return error;
  }

  function handleSubmit(
    actions: FormikHelpers<{ cpf: string }>,
    data: { cpf: string }
  ) {
    api
      .post("/auth/profile", data)
      .then((res) => {
        actions.setSubmitting(false);
        actions.resetForm();
        setDataAccount(res.data.account_balance);
        setIsViewTable(true);
      })
      .catch((err) => {
        actions.setSubmitting(false);
        setIsViewTable(false);
        if (err.response.data.message === "User not found.") {
          return actions.setStatus({ success: "Usuário não encontrado." });
        }
        actions.setStatus({ success: "Erro ao realizar o consulta." });
      });
  }
  return (
    <>
      {!isViewTable ? (
        <Formik
          initialValues={{ cpf: "" }}
          onSubmit={(values, actions) => {
            const data = {
              cpf: values.cpf,
            };
            handleSubmit(actions, data);
          }}
        >
          {({ isSubmitting, status }) => (
            <Form>
              <Field name="cpf" validate={validateCPF}>
                {({ field, form }: any) => (
                  <FormControl isInvalid={form.errors.cpf && form.touched.cpf}>
                    <FormLabel htmlFor="cpf">CPF da conta:</FormLabel>
                    <Input {...field} id="cpf" placeholder="Digite aqui..." />
                    <FormErrorMessage>{form.errors.cpf}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>
              <div>{status ? status.success : ""}</div>
              <Button
                mt={4}
                colorScheme="teal"
                isLoading={isSubmitting}
                type="submit"
              >
                Realizar consulta
              </Button>
            </Form>
          )}
        </Formik>
      ) : (
        <TableData dataAccount={dataAccount} />
      )}
    </>
  );
}

function TableData({ dataAccount }: { dataAccount: ITableData }) {
  const date = new Date();

  return (
    <>
      <Text>
        Seu saldo é:{" "}
        <strong>R$ {dataAccount.balance.toFixed(2).replace(".", ",")}</strong>
      </Text>
      <TableContainer>
        <Table variant="striped" colorScheme="gray.600">
          <TableCaption>
            Informações atualizadas em:{" "}
            {date.toLocaleDateString("pt-BR", {
              day: "2-digit",
              month: "2-digit",
              year: "numeric",
              hour: "2-digit",
              minute: "2-digit",
              second: "2-digit",
            })}
          </TableCaption>
          <Thead>
            <Tr>
              <Th>Data e Hora</Th>
              <Th isNumeric>Valor</Th>
            </Tr>
          </Thead>
          <Tbody>
            {dataAccount.transactions.map((_, index) => {
              let date = new Date(_.createdAt);
              console.log(_.value);
              return (
                <Tr key={_.id}>
                  <Td>
                    {date.toLocaleDateString("pt-BR", {
                      day: "2-digit",
                      month: "2-digit",
                      year: "numeric",
                      hour: "2-digit",
                      minute: "2-digit",
                      second: "2-digit",
                    })}
                  </Td>
                  <Td isNumeric>
                    <Text textColor={_.type === "debit" ? "red" : ""}>
                      R$ {_.value.toFixed(2).replace(".", ",")}
                    </Text>
                  </Td>
                </Tr>
              );
            })}
          </Tbody>
          <Tfoot>
            {/* <Tr>
              <Th></Th>
              <Th isNumeric></Th>
            </Tr> */}
          </Tfoot>
        </Table>
      </TableContainer>
    </>
  );
}
