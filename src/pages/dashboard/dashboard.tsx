import {
  Box,
  Button,
  Center,
  Container,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  SimpleGrid,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { useRef } from "react";
import { BalanceModal } from "../../components/balanceModal/balanceModal";
import { DepositModal } from "../../components/depositModal/depositModal";
import { WithdrawModal } from "../../components/withdrawModal/withdrawModal";

export default function Dashboard() {
  return (
    <>
      <Box p={"4"} mb={"8"}>
        <Heading>Konv Mini Bank</Heading>
        <Text fontSize="xl">
          Bem vindo ao Konv Mini Bank, o banco mais simples e rápido do mundo.
        </Text>
      </Box>
      <Container maxW={"5xl"}>
        <SimpleGrid columns={[1, 2, 3]} spacing="40px">
          <Box bg="tomato" h={"200px"} borderRadius="5px">
            <Center h="100%">
              <Text fontSize="xl">
                <CustomModalBox operation="Depósito" />
              </Text>
            </Center>
          </Box>
          <Box bg="tomato" h={"200px"} borderRadius="5px">
            <Center h="100%">
              <Text fontSize="xl">
                <CustomModalBox operation="Saque" />
              </Text>
            </Center>
          </Box>
          <Box bg="tomato" h={"200px"} borderRadius="5px">
            <Center h="100%">
              <Text fontSize="xl">
                <CustomModalBox operation="Extrato e Saldo" />
              </Text>
            </Center>
          </Box>
        </SimpleGrid>
      </Container>
    </>
  );
}

interface CustomModalBoxProps {
  operation: string;
}

function CustomModalBox({ operation }: CustomModalBoxProps) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = useRef(null);

  const switchOp = (operation: string) => {
    switch (operation) {
      case "Depósito":
        return <DepositModal />;
      case "Saque":
        return <WithdrawModal />;
      case "Extrato e Saldo":
        return <BalanceModal />;
      default:
        return <></>;
    }
  };

  return (
    <>
      <Button w="200px" ref={btnRef} onClick={onOpen}>
        {operation}
      </Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{operation}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>{switchOp(operation)}</ModalBody>
          <ModalFooter>
            {/* <Button onClick={onClose}>Fechar</Button> */}
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
