import {
  Box,
  Button,
  Center,
  Container,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
} from "@chakra-ui/react";
import { Field, Form, Formik } from "formik";
import { api } from "../../services/api";
import { isValidCpf } from "../../utils/validateCPF";

export function Login() {
  function validateName(value: string) {
    let error;
    if (!value) {
      error = "CPF é requerido.";
    } else if (!isValidCpf(value)) {
      error = "CPF inválido.";
    }
    return error;
  }

  return (
    <Container maxW="2xl">
      <Heading>Konv Mini Bank</Heading>
      <Center h="100vh">
        <Box bg="blue.200" p="10" borderRadius="lg">
          <Formik
            initialValues={{ cpf: "" }}
            onSubmit={(values, actions) => {
              const data = {
                cpf: values.cpf,
                password: "123456",
              };
              api
                .post("/auth/login", data)
                .then((res) => {
                  alert(JSON.stringify(res.data, null, 2));
                  actions.setSubmitting(false);
                  //   console.log("Login realizado com sucesso!");
                })
                .catch((err) => {
                  //   console.log(err);
                });
            }}
          >
            {(props) => (
              <Form>
                <Field name="cpf" validate={validateName}>
                  {({ field, form }: any) => (
                    <FormControl
                      isInvalid={form.errors.cpf && form.touched.cpf}
                    >
                      <FormLabel htmlFor="cpf">Insira seu CPF:</FormLabel>
                      <Input {...field} id="cpf" placeholder="Digite aqui..." />
                      <FormErrorMessage>{form.errors.cpf}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
                <Button
                  mt={4}
                  colorScheme="teal"
                  isLoading={props.isSubmitting}
                  type="submit"
                >
                  Entrar
                </Button>
              </Form>
            )}
          </Formik>
        </Box>
      </Center>
    </Container>
  );
}
