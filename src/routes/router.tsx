import { RequireAuth, useAuthUser } from "react-auth-kit";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Dashboard from "../pages/dashboard/dashboard";
import { Login } from "../pages/login/login";

export default function Router() {
  const auth = useAuthUser();

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/dashboard" element={<Dashboard />} />

        {/* <Route
          path="/dashboard"
          element={
            <RequireAuth loginPath={"/login"}>
              <Dashboard />
            </RequireAuth>
          }
        /> */}
        <Route path="*" element={<Navigate to={"/login"} replace />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
