import "./App.css";
import { AuthProvider } from "react-auth-kit";
import { ChakraProvider } from "@chakra-ui/react";
import Router from "./routes/router";

function App() {
  return (
    <ChakraProvider>
      <AuthProvider
        authType={"cookie"}
        authName={"_auth"}
        cookieDomain={window.location.hostname}
        cookieSecure={window.location.protocol === "https:"}
      >
        <Router />
      </AuthProvider>
    </ChakraProvider>
  );
}

export default App;
